import 'package:rxdart/rxdart.dart';
import 'package:thenewsapp/model/article_response.dart';
import 'package:thenewsapp/repository/repository.dart';

class SearchBloc {
  final NewsRepository _repository = NewsRepository();
  BehaviorSubject<ArticleResponse> _subject = BehaviorSubject<ArticleResponse>();

  search(String value) async {
    ArticleResponse response = await _repository.search(value);
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<ArticleResponse> get subject => _subject;
}

final searchBloc = SearchBloc();