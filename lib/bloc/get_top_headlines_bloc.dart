import 'package:rxdart/rxdart.dart';
import 'package:thenewsapp/model/article_response.dart';
import 'package:thenewsapp/repository/repository.dart';

class GetTopHeadlinesBloc {
  final NewsRepository _repository = NewsRepository();
  BehaviorSubject<ArticleResponse> _subject = BehaviorSubject<ArticleResponse>();

  getHeadlines() async {
    ArticleResponse response = await _repository.getTopHeadlines();
    _subject.sink.add(response);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<ArticleResponse> get subject => _subject;
}

final getTopHeadlinesBloc = GetTopHeadlinesBloc();