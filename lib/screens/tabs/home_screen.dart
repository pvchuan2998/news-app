import 'package:flutter/material.dart';
import 'package:thenewsapp/widgets/headline_slider.dart';
import 'package:thenewsapp/widgets/hot_news.dart';
import 'package:thenewsapp/widgets/top_channel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        HeadlineSlider(),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "Top Channels",
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 17.0),
          ),
        ),
        TopChannel(),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "Hot News",
            style: TextStyle(
                color: Colors.black,
                fontSize: 17.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        HotNews(),
      ],
    );
  }
}
